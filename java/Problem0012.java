import java.util.Map;

public class Problem0012 {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        int n = 1,a,b;
        while (true) {
            if (n % 2 == 0) {
                a = n / 2;
                b = n + 1;
            } else {
                a = (n + 1) / 2;
                b = n;
            }
            int factorCount = countFactors(Common.factorize(a))
                * countFactors(Common.factorize(b));
            if (factorCount > 500) {
                System.out.println(a*b);
                break;
            }
            n++;
        }
        long end = System.currentTimeMillis();
        System.out.println("time (ms) = " + (end-start));
    }

    private static int countFactors(Map<Integer,Integer> factorization) {
        int factorCount = 1;
        for (Integer i : factorization.values()) {
            factorCount *= i + 1;
        }
        return factorCount;
    }
}
