import java.util.BitSet;

public class Problem0046 {
    public static void main(String[] args) {
        // Guess an upper limit to the answer
        final int limit = 10000;
        BitSet primes = Common.primeSieve(limit);
        // Loop through odd composites
        for (int c = primes.nextClearBit(9); c < limit;
                c = primes.nextClearBit(c + 1)) {
            if (c % 2 == 0) {
                continue;
            }
            boolean found = true;
            // Subtract twice squares and test for primality 
            int s = 1;
            while (2 * s * s < c) {
                if (primes.get(c - 2 * s * s)) {
                    found = false;
                    break;
                }
                s++;
            }
            if (found) {
                System.out.println(c);
                break;
            }
        }
    }
}
