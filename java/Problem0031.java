public class Problem0031 {
    public static void main(String[] args) {
        final int[] coins = new int[] { 1, 2, 5, 10, 20, 50, 100, 200 };
        final int v = 200;
        int[][] ways = new int[v + 1][coins.length + 1];
        for (int i = 0; i < v + 1; i++) ways[i][0] = 0;
        for (int i = 0; i < coins.length + 1; i++) ways[0][i] = 1;
        for (int i = 1; i < v + 1; i++) {
            ways[i][0] = 0;
            for (int j = 1; j < coins.length + 1; j++) {
                ways[i][j] = ways[i][j - 1];
                int ii = i - coins[j - 1];
                if (ii >= 0) {
                    ways[i][j] += ways[ii][j];
                }
            }
        }
        System.out.println(ways[v][coins.length]);
    }
}
