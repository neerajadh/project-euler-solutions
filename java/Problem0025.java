import java.math.BigInteger;

public class Problem0025 {
    public static void main(String[] args) {
        BigInteger a = BigInteger.valueOf(0L);
        BigInteger b = BigInteger.valueOf(1L);
        BigInteger c;
        int index = 1;
        while (true) {
            c = a.add(b);
            a = b;
            b = c;
            index++;
            if (c.toString().length() >= 1000) {
                System.out.println(index);
                break;
            }
        }
    }
}
