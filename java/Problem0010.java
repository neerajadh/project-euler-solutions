import java.util.BitSet;

public class Problem0010 {
    public static void main(String[] args) {
        final int limit = 2000000; 
        BitSet primes = Common.primeSieve(limit);
        int index = 2;
        long sum = 0;
        while (index != -1) {
            sum += index;
            index = primes.nextSetBit(index + 1);
        }
        System.out.println(sum);
    }
}
