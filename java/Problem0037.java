import java.util.LinkedList;
import java.util.Arrays;

public class Problem0037 {
    public static void main(String[] args) {
        LinkedList<Long> queue = new LinkedList<>();
        long truncatableSum = 0L;
        queue.addAll(Arrays.asList(3L,7L));
        while (queue.size() > 0) {
            long size = queue.size();
            for (long i = 0; i < size; i++) {
                long prime = queue.remove();
                int d = Common.digitCount(prime);
                for (Long j : Arrays.asList(1L,2L,3L,5L,7L,9L)) {
                    long newPrime = prime + j * (long)Math.pow(10,d);
                    if (Common.isPrime((long)newPrime).getFirst()) {
                        queue.add(newPrime);
                    }
                }
                if (isRightTruncatable(prime) && d > 1) {
                    truncatableSum += prime;
                }
            }
        }
        System.out.println(truncatableSum);
    }

    private static boolean isRightTruncatable(long p) {
        while (p > 0L) {
            if (!Common.isPrime(p).getFirst()) return false;
            p /= 10L;
        }
        return true;
    }
}
