import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Problem0042 {
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(new File("../data","p042_words.txt"));
            scanner.useDelimiter(",");
            List<String> words = new ArrayList<>();
            while (scanner.hasNext()) {
                String quotedWord = scanner.next();
                words.add(quotedWord.substring(1,quotedWord.length() - 1));
            }
            int triangleNumbers = 0;
            for (int i = 0 ; i < words.size(); i++) {
                int charSum = 0;
                for (int j = 0; j < words.get(i).length(); j++) {
                    charSum += (int) (words.get(i).charAt(j) - 'A' + 1);
                }
                charSum *= 2;
                double sumRoot = Math.sqrt((double)charSum);
                if ((int)Math.floor(sumRoot) *
                        (int)Math.ceil(sumRoot) == charSum
                        && (int)sumRoot * (int)sumRoot != charSum) {
                    triangleNumbers++;
                }
            }
            System.out.println(triangleNumbers);
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }
}
