public class Problem0006 {
    public static void main(String[] args) {
        final int N = 100;
        // For such a small value of N, the answer can be easily evaluated
        // in a closed-form expression without causing 32-bit integer overflow.
        // Result = ((N^2)*((N+1)^2)/4) - (N*(N+1)*(2*N+1)/6)
        // But here is a straightforward summing solution
        int sum = 0;
        for (int i = 1; i <= N; i++) {
            for (int j = i + 1; j <= N; j++) {
                sum += 2*i*j;
            }
        }
        assert sum == (Math.pow(N,2)*Math.pow((N+1),2)/4) - (N*(N+1)*(2*N+1)/6);
        System.out.println(sum);
    }
}
