import java.util.Arrays;

public class Problem0015 {
    public static void main(String[] args) {
        // You can easily derive a closed-form expression for this problem
        // if you think about it. In a 20x20 grid, to reach the bottom right
        // from the top left, you will need 20 right steps and 20 down steps
        // for every path. So essentially we want to know how many ways there
        // are to combine 20 right-steps and 20 down-steps to make a total of
        // forty steps. Thinking of it another way, the question is the same
        // as asking 'How many ways can you choose 20 objects from 40 unique
        // objects?', which is C(40,20). The 40 unique objects in this case
        // are the steps in the 40-step path. The 20 you have to choose are
        // the 20 down (or right, equivalently) steps.
        //
        // But this problem can be solved in another interesting way, with
        // dynamic programming.
        
        long[][] ways = new long[21][21];

        ways[ways.length - 1][ways[0].length - 1] = 1;
        for (int i = ways.length - 1; i >= 0; i--) {
            for (int j = ways[0].length -1; j >= 0; j--) {
                if (i == ways.length - 1 && j < ways[0].length - 1) {
                    ways[i][j] = ways[i][j + 1];
                } else if (j == ways[0].length - 1 && i < ways.length - 1) {
                    ways[i][j] = ways[i + 1][j];
                } else if (i < ways.length - 1 && j < ways[0].length - 1) {
                    ways[i][j] = ways[i + 1][j] + ways[i][j + 1];
                }
            }
        }
        System.out.println(ways[0][0]);
    }
}
