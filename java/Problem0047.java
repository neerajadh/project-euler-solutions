import java.util.SortedMap;

public class Problem0047 {
    public static void main(String[] args) {
        int i = 647;
        int j = i + 1;
        int k = j + 1;
        int l = k + 1;
        SortedMap<Integer,Integer> iFactors = Common.factorize(i);
        SortedMap<Integer,Integer> jFactors = Common.factorize(j);
        SortedMap<Integer,Integer> kFactors = Common.factorize(k);
        SortedMap<Integer,Integer> lFactors = Common.factorize(l);
        while (true) {
            if (iFactors.size() == 4 && jFactors.size() == 4
                    && kFactors.size() == 4 && lFactors.size() == 4) {
                System.out.println(i);
                break;
            }
            i = j;
            j = k;
            k = l;
            l += 1;
            iFactors = jFactors;
            jFactors = kFactors;
            kFactors = lFactors;
            lFactors = Common.factorize(l);
        }
    }
}
