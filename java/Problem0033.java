public class Problem0033 {
    public static void main(String[] args) {
        int nproduct = 1, dproduct = 1;
        for (int n = 11; n < 100; n++) {
            for (int d = n + 1; d < 100; d++) {
                int shared = sharedDigit(n,d);
                if (shared > 0) {
                    int n_ = removeDigit(n,shared);
                    int d_ = removeDigit(d,shared);
                    if (n_ * d == d_ * n) {
                        nproduct *= n;
                        dproduct *= d;
                        System.out.println(String.format("%d/%d",n,d));
                    }
                }
            }
        }
        int gcd = Common.gcd(nproduct,dproduct);
        System.out.println(dproduct/gcd);
    }

    /** Remove the digit d from a two-digit number a.
     */
    private static int removeDigit(int a, int d) {
        if (a % 10 == d) {
            return a / 10;
        }
        if (a / 10 == d) {
            return a % 10;
        }
        return a;
    }

    /** Get the shared digit between two 2-digit numbers, -1 if 
     * no digit is shared.
     */
    private static int sharedDigit(int a, int b) {
        int a0 = a % 10, a1 = (a / 10) % 10;
        int b0 = b % 10, b1 = (b / 10) % 10;
        if (a0 == b0 || a0 == b1) return a0;
        if (a1 == b0 || a1 == b1) return a1; 
        return -1;
    }
}
