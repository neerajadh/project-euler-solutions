public class Problem0001 {
    public static void main(String[] args) {
        // There are 333 multiples of 3 below 1000
        int mults3 = 3*((333*334)/2);
        // 199 multiples of 5 below 1000
        int mults5 = 5*((199*200)/2);
        // And 66 multiples of 15 below 1000
        int mults15 = 15*((66*67)/2);
        // Add the sums and subtract the redundant multiples of 15
        int result = mults3 + mults5 - mults15;
        System.out.println(result);
    }
}
