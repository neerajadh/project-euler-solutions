public class Problem0019 {
    public static void main(String[] args) {
        int dayOfWeek = 1, day = 0, month = 0, year = 1900,
            firstSundays = 0, monthDays = daysInMonth(0,1900);
        while (year < 2001) {
            if (day == 0 && dayOfWeek == 0 && year > 1900) {
                firstSundays++;
            }
            dayOfWeek = (dayOfWeek + 1) % 7;
            day = (day + 1) % monthDays;
            if (day == 0) {
                month = (month + 1) % 12;
                if (month == 0) year++;
                monthDays = daysInMonth(month,year);
            }
        }
        System.out.println(firstSundays);
    }

    private static int daysInMonth(int month, int year) {
        switch (month) {
            case 0:
            case 2:
            case 4:
            case 6:
            case 7:
            case 9:
            case 11:
                return 31;
            case 1:
                return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
                    ? 29 : 28;
            default:
                return 30;
        }
    }
}
