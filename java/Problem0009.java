public class Problem0009 {
    public static void main(String[] args) {
        outer: for (int a = 1; a < 1000; a++) {
            for (int b = a + 1; b < 1000; b++) {
                int csq = a * a + b * b;
                int c = (int)Math.sqrt(csq);
                if (c * c == csq) {
                    if (a + b + c == 1000) {
                        System.out.println(String.format("a=%d b=%d c=%d",a,b,c));
                        System.out.println(String.format("abc=%d",a*b*c));
                        break outer;
                    }
                }
                if (a + b + c > 1000) {
                    break;
                }
            }
        }
    }
}
