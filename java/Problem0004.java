public class Problem0004 {
    public static void main(String[] args) {
        final int start = 999, end = 100;
        int a = start, b = start, d = 0; 
        int product = 0;
        while (d < (start - end + 1)) {
            product = a * b;
            if (isPalindrome(product)) {
                System.out.println(String.format("%d x %d = %d",a,b,product));
                break;
            }
            // Enumerate a,b in order of decreasing product.
            // Starting from N*N, we keep a count (d) of the amout we have
            // subtracted from both numbers, evaluate all products for a
            // fixed d and then decrease d. For a fixed d, we enumerate pairs
            // starting from either n*n (for even d) or n*(n-1) (for odd d).
            if (a == start) {
                d++;
                a = start - (d / 2);
                b = a;
                if (d % 2 == 1) b--;
            } else {
                a++;
                b--;
            }

        }
    }

    private static boolean isPalindrome(int num) {
        int collector = 0;
        if (num % 10 == 0) return false;
        while (num > 0) {
            // For numbers with even number of digits
            if (collector == num) return true;
            collector = collector * 10 + num % 10;
            // For numbers with odd number of digits
            if (collector == num) return true;
            num /= 10;
        }
        return false;
    }
}
