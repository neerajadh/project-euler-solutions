import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;

public class Problem0022 {
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(new File("../data","p022_names.txt"));
            scanner.useDelimiter(",");
            List<String> names = new ArrayList<>();
            while (scanner.hasNext()) {
                String quotedName = scanner.next();
                names.add(quotedName.substring(1,quotedName.length() - 1));
            }
            Collections.sort(names);
            int sum = 0;
            for (int i = 0 ; i < names.size(); i++) {
                int nameValue = 0;
                for (int j = 0; j < names.get(i).length(); j++) {
                    nameValue += (int)(names.get(i).charAt(j) - 'A' + 1);
                }
                sum += nameValue * (i + 1);
            }
            System.out.println(sum);
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }
}
