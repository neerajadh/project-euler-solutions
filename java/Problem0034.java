import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Problem0034 {

    private static int[] factorials;
    static {
        factorials = new int[10];
        factorials[0] = 1;
        for (int i = 1; i < factorials.length; i++) {
            factorials[i] = factorials[i - 1] * i;
        }
    }

    public static void main(String[] args) {
        // Here -1 is a special value that denotes leading zeros
        // Because we need to treat leading zeros and non-leading
        // zeros differently
        List<Integer> elements = Arrays.asList(-1,0,1,2,3,4,5,6,7,8,9);
        int r = 7, sum = 0;
        List<List<Integer>> multiCombs = Common.multiCombinations(elements,r);
        for (List<Integer> combination : multiCombs) {
            int digitFactSum = factorialSum(combination);
            if (digitFactSum > 2 && digitSet(digitFactSum,r).equals(combination)) {
                sum += digitFactSum;
                System.out.println(combination);
            }
        }
        System.out.println("Sum = " + sum);
    }

    private static List<Integer> digitSet(int n, int size) {
        ArrayList<Integer> set = new ArrayList<>();
        while (n > 0) {
            set.add(n % 10);
            size--;
            n /= 10;
        }
        while (size > 0) {
            set.add(-1);
            size--;
        }
        Collections.sort(set);
        return set;
    }

    private static int factorialSum(List<Integer> numbers) {
        int sum = 0, i = 0;
        for (; i < numbers.size(); i++) {
            if (numbers.get(i) >= 0)
                sum += factorials[numbers.get(i)];
        }
        return sum;
    }
}
