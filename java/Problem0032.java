import java.util.Arrays;
import java.util.HashSet;

public class Problem0032 {
    public static void main(String[] args) {
        int sum = 0;
        HashSet<Integer> pandigitalProducts = new HashSet<>();
        for (int a = 2; a < 9999; a++) {
            for (int b = 2; b < a; b++) {
                int c = a * b;
                if (digits(a) + digits(b) + digits(c) > 9) break;
                if (isPandigital(a,b,c) && !pandigitalProducts.contains(c)) {
                    sum += c;
                    pandigitalProducts.add(c);
                }
            }
        }
        System.out.println(sum);
    }

    private static int digits(int n) {
        int d = 0;
        while (n > 0) {
            d++;
            n /= 10;
        }
        return d;
    }

    private static boolean isPandigital(int a, int b, int c) {
        int digits = 0;
        int[] digitCounts = new int[10];
        for (int i : Arrays.asList(a,b,c)) {
            while (i > 0) {
                digits++;
                digitCounts[i % 10]++;
                if (i % 10 == 0 || digitCounts[i % 10] > 1) return false;
                i /= 10;
            }
        }
        return digits == 9;
    }
}
