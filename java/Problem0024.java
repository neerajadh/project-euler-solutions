import java.util.Arrays;
import java.util.ArrayList;

public class Problem0024 {

    private static int[] fact;
    static {
        fact = new int[10];
        fact[0] = 1;
        for (int i = 1; i < 10; i++) {
            fact[i] = i * fact[i-1];
        }
    }

    public static void main(String[] args) {
        int n = 1000000 - 1;
        ArrayList<Character> remainingElements = new ArrayList<>(
                Arrays.asList('0','1','2','3','4','5','6','7','8','9')
        );
        StringBuilder permutation = new StringBuilder();
        for (int i = 10; i > 0; i--) {
            int nextIndex = n/fact[i-1];
            n -= fact[i-1] * nextIndex;
            permutation.append(remainingElements.remove(nextIndex));
        }
        System.out.println(permutation.toString());
    }
}
