import java.util.BitSet;

public class Problem0003 {
    public static void main(String[] args) {
        long num = 600851475143L;
        // long num = 2L*87178291199L;
        long start = System.nanoTime();
        withoutPrimalityCheckOptimized(num);
        long check1 = System.nanoTime();
        withoutPrimalityCheck(num);
        long check2 = System.nanoTime();
        usingPrimalityCheck(num);
        long end = System.nanoTime();
        // The optimized version outshines the other methods when the number
        // contains a very large prime.
        System.out.println("without primality check, optimized (ns) = " + (check1-start));
        System.out.println("without primality check (ns) = " + (check2-check1));
        System.out.println("with primality check (ns) = " + (end-check2));
    }

    private static void withoutPrimalityCheckOptimized(long num) {
        long divisor = 0;
        while (true) {
            divisor += divisor == 2 ? 1 : 2;
            if (divisor * divisor > num) {
                divisor = num;
                break;
            }
            while (num % divisor == 0)  num /= divisor;
            if (num == 1) break;
        }
        System.out.println(divisor);
    }

    private static void withoutPrimalityCheck(long num) {
        long divisor = 0;
        while (true) {
            divisor += divisor == 2 ? 1 : 2;
            while (num % divisor == 0)  num /= divisor;
            if (num == 1) break;
        }
        System.out.println(divisor);
    }

    public static void usingPrimalityCheck(long num) {
        long divisor;
        while (true) {
            Common.Pair<Boolean,Long> isPrime = Common.isPrime(num);
            if (isPrime.getFirst()) {
                break;
            }
            divisor = isPrime.getSecond();
            while (num % divisor == 0)  num /= divisor;
        }
        System.out.println(num);
    }
}
