import java.util.Arrays;

public class Problem0021 {
    public static void main(String[] args) {
        final int N = 10000;
        int[] divisorSums = new int[N];
        Arrays.fill(divisorSums,0);
        for (int i = 1; i < N; i++) {
            int j = 2*i;
            while (j < N) {
                divisorSums[j] += i;
                j += i;
            }
        }
        int amicableSum = 0;
        for (int i = 2; i < N; i++) {
            if (divisorSums[i] < N && (divisorSums[divisorSums[i]] == i)
                    && i != divisorSums[i]) {
                amicableSum += i;
            }
        }
        System.out.println(amicableSum);
    }
}
