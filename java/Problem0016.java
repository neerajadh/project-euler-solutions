import java.math.BigInteger;

public class Problem0016 {
    public static void main(String[] args) {
        BigInteger power = (BigInteger.valueOf(2L)).pow(1000);
        int sum = 0;
        char[] chars = power.toString().toCharArray();
        for (char ch : chars) {
            sum += (int)(ch - '0');
        }
        System.out.println(sum);
    }
}
