import java.util.List;
import java.util.ArrayList;

public class Problem0038 {
    public static void main(String[] args) {
        int largest  = 0;
        for (int m = 1; m < 100000; m++) {
            List<Integer> multiples = new ArrayList<>();
            if (hasRepeatedDigits(m)) continue;
            multiples.add(m);
            for (int i = 2; i <= 9; i++) {
                multiples.add(i * m);
                if (!isPandigital(multiples)) {
                    multiples.remove(multiples.size() - 1);
                    break;
                }
            }
            if (multiples.size() == 1) continue;
            int concatenation = concatenate(multiples);
            if (concatenation > largest) {
                largest = concatenation;
            }
        }
        System.out.println(largest);
    }

    private static boolean isPandigital(List<Integer> numbers) {
        int digits = 0;
        int[] digitCounts = new int[10];
        for (int i : numbers) {
            while (i > 0) {
                digits++;
                digitCounts[i % 10]++;
                if (i % 10 == 0 || digitCounts[i % 10] > 1) return false;
                i /= 10;
            }
        }
        return digits == 9;
    }

    private static boolean hasRepeatedDigits(int number) {
        int[] digitCounts = new int[10];
        while (number > 0) {
            digitCounts[number % 10]++;
            if (digitCounts[number % 10] > 1) return true;
            number /= 10;
        }
        return false;
    }

    private static int concatenate(List<Integer> numbers) {
        int concatenation = 0;
        for (Integer number : numbers) {
            concatenation = concatenation * (int)Math.pow(10,
                    Common.digitCount((long)number)) + number;
        }
        return concatenation;
    }
}
