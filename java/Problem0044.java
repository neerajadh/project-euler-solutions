public class Problem0044 {
    public static void main(String[] args) {
        int lowestDifference = Integer.MAX_VALUE;
        outer:
        for (int j = 2; ; j++) {
            for (int k = j - 1; k >= 1; k--) {
                int difference = pentagonalDifference(j, k);
                if (difference > lowestDifference) {
                    if (k == j - 1) {
                        break outer;
                    } else {
                        break;
                    }
                }
                if (isSumPentagonal(j,k) && isDifferencePentagonal(j,k)) {
                    if (difference < lowestDifference) {
                        lowestDifference = difference;
                    }
                }
            }
        }
        System.out.println(lowestDifference);
    }

    private static boolean isSumPentagonal(int j, int k) {
        int s = 3 * (j * j + k * k) - (j + k);
        s = 1 + 12 * s;
        int root = (int)Math.sqrt(s);
        return root * root == s && (1 + root) % 6 == 0;
    }

    private static boolean isDifferencePentagonal(int j, int k) {
        int s = 3 * (j * j - k * k) - (j - k);
        s = 1 + 12 * s;
        int root = (int)Math.sqrt(s);
        return root * root == s && (1 + root) % 6 == 0;
    }

    private static int pentagonalDifference(int j, int k) {
        if (j < k) {
            int t = j;
            j = k;
            k = t;
        }
        return (3 * (j * j - k * k) - (j - k)) / 2;
    }
}

