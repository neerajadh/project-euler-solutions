import java.util.BitSet;

public class Problem0035 {
    public static void main(String[] args) {
        final int limit = 1000000;
        BitSet primes = Common.primeSieve(limit);
        int count = 0;
        for (int i = 1; i < limit;) {
            i = primes.nextSetBit(i + 1);
            if (i < 0) break;
            int[] rotations = getRotations(i);
            boolean circular = true;
            for (int r : rotations) {
                if (!primes.get(r)) {
                    circular = false;
                }
            }
            if (circular) {
                for (int r : rotations) {
                    count++;
                    primes.clear(r);
                    if (rotations.length > 1 && rotations[0] == rotations[1])
                        break;
                }
            }
        }
        System.out.println(count);
    }

    private static int[] getRotations(int num) {
        int[] digits = new int[numDigits(num)];
        int[] rotations = new int[digits.length];
        for (int i = 0; i < digits.length; i++) {
            digits[digits.length - i - 1] = num % 10;
            num /= 10;
        }
        for (int i = 0; i < digits.length; i++) {
            int j = i;
            int n = 0;
            do {
                n = n * 10 + digits[j];
                j = (j + 1) % digits.length;
            } while (j != i);
            rotations[i] = n;
        }
        return rotations;
    }

    private static int numDigits(int num) {
        int d = 0;
        while (num > 0) {
            d++;
            num /= 10;
        }
        return d;
    }
}
