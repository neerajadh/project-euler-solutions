import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Problem0030 {
    public static void main(String[] args) {
        List<Integer> elements = Arrays.asList(0,1,2,3,4,5,6,7,8,9);
        int r = 6, sum = 0, power = 5;
        List<List<Integer>> multiCombs = Common.multiCombinations(elements,r);
        for (List<Integer> combination : multiCombs) {
            int digitPowerSum = 0;
            for (Integer digit : combination) {
                digitPowerSum += (int)Math.pow(digit,power);
            }
            if (digitPowerSum > 1 && digitSet(digitPowerSum,r)
                    .equals(combination)) {
                sum += digitPowerSum;
                System.out.println(digitPowerSum);
            }
        }
        System.out.println("Sum = " + sum);
    }

    private static List<Integer> digitSet(int n, int size) {
        ArrayList<Integer> set = new ArrayList<>();
        while (size > 0) {
            set.add(n % 10);
            size--;
            n /= 10;
        }
        Collections.sort(set);
        return set;
    }
}
