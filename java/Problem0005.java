public class Problem0005 {
    public static void main(String[] args) {
        int result = 1;
        for (int i = 2; i < 20; i++) {
            if (result % i != 0) {
                result *= i / Common.gcd(result,i);
            }
        }
        System.out.println(result);
    }
}
