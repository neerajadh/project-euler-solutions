import java.util.List;
import java.util.ArrayList;

public class Problem0014 {
    public static void main(String[] args) {
        List<Long> chainLengths = new ArrayList<>();
        chainLengths.add(0L);
        chainLengths.add(1L);
        long longestLength = 1;
        long longestStart = 1;
        long num, length;
        for (int i = 2; i < 1000000; i++) {
            num = (long)i;
            long steps = 0L;
            while (true) {
                if (num < i) {
                    chainLengths.add(chainLengths.get((int)num) + steps);
                    if (chainLengths.get(i) > longestLength) {
                        longestLength = chainLengths.get(i);
                        longestStart = i;
                    }
                    break;
                }
                if (num % 2 == 0) {
                    num /= 2;
                } else {
                    num = 3 * num + 1;
                }
                steps++;
            }
        }
        System.out.println(longestLength);
        System.out.println(longestStart);
    }
}
