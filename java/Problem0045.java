public class Problem0045 {
    public static void main(String[] args) {
        int count = 0;
        for (long j = 2L; count < 2; j++) {
            long t = (j * (j + 1L)) / 2L;
            if (isPentagonal(t) && isHexagonal(t)) {
                System.out.println(t);
                count++;
            }
        }
    }
    
    private static boolean isPentagonal(long n) {
        long s = 1L + 24L * n;
        long root = (long)Math.sqrt(s);
        return root * root == s && (1L + root) % 6L == 0L;
    }

    private static boolean isHexagonal(long n) {
        long s = 1L + 8L * n;
        long root = (long)Math.sqrt(s);
        return root * root == s && (1L + root) % 4L == 0L;
    }
}
