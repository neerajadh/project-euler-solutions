import java.util.function.Consumer;

public class Problem0043 {
    public static void main(String[] args) {
        final Common.MutableLong sum = new Common.MutableLong();
        Common.sjtPermutations(10, permutation -> {
            long entireNumber = Common.digitArrayToNumber(permutation, 0, 10, 0);
            if (entireNumber < 1000000000) {
                return;
            }
            boolean allConditions =
                Common.digitArrayToNumber(permutation, 1, 4, 0) % 2 == 0 &&
                Common.digitArrayToNumber(permutation, 2, 5, 0) % 3 == 0 &&
                Common.digitArrayToNumber(permutation, 3, 6, 0) % 5 == 0 &&
                Common.digitArrayToNumber(permutation, 4, 7, 0) % 7 == 0 &&
                Common.digitArrayToNumber(permutation, 5, 8, 0) % 11 == 0 &&
                Common.digitArrayToNumber(permutation, 6, 9, 0) % 13 == 0 &&
                Common.digitArrayToNumber(permutation, 7, 10, 0) % 17 == 0;
            if (allConditions)
                sum.value += entireNumber;
        });
        System.out.println(sum.value);
    }
}
