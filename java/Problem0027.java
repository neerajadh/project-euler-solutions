import java.util.BitSet;

public class Problem0027 {
    public static void main(String[] args) {
        final int limit_b = 1000, limit_a = 1000;
        BitSet primes = Common.primeSieve(limit_b);
        int b = primes.nextSetBit(0), max = 0, maxAB = 0;
        do {
            for (int a = - (limit_a - 1); a < limit_a; a++) {
                int n = 0;
                while (Common.isPrime((long)(n * n + a * n + b)).getFirst()) n++;
                if (max < n) {
                    max = n;
                    maxAB = a * b;
                }
            }
            b = primes.nextSetBit(b + 1);
        } while (b < limit_b && b >= 0);
        System.out.println(maxAB);
    }
}
