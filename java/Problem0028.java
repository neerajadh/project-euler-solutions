public class Problem0028 {
    public static void main(String[] args) {
        // A closed form expression for the answer can be easily derived
        // by observing the diagonal numbers and doing some algebra. Here
        // I have summed using a loop because I'm feeling a bit lazy to do 
        // the paper algebra.
        final int N = 1001;
        int sum = 1;
        for (int i = 1; i <= (N / 2); i++) {
            sum += 4 * (2 * i + 1) * (2 * i + 1) - 6 * (2 * i + 1) + 6;
        }
        System.out.println(sum);
    }
}
