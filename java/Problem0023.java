import java.util.BitSet;
import java.util.ArrayList;

public class Problem0023 {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        final int N = 28124;
        int[] divisorSums = new int[N];
        for (int i = 1; i < N; i++) {
            int j = 2*i;
            while (j < N) {
                divisorSums[j] += i;
                j += i;
            }
        }
        ArrayList<Integer> abundants = new ArrayList<Integer>();
        for (int i = 1; i < N; i++) {
            if (divisorSums[i] > i) {
                abundants.add(i);
            }
        }
        int abundantSumsSum = 0;
        BitSet isAbundantSum = new BitSet(N);
        for (int i = 0; i < abundants.size(); i++) {
            for (int j = i; j < abundants.size(); j++) {
                int sum = abundants.get(i) + abundants.get(j);
                if (sum >= N) {
                    break;
                }
                if (!isAbundantSum.get(sum)) {
                    abundantSumsSum += sum;
                    isAbundantSum.set(sum);
                }
            }
        }
        int nonAbundantSumsSum = (N*(N-1))/2 - abundantSumsSum;
        long end = System.currentTimeMillis();
        System.out.println(nonAbundantSumsSum);
        System.out.println("Time (ms) = " + (end-start));
    }
}
