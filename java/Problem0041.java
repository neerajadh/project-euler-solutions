import java.util.function.Consumer;

public class Problem0041 {

    public static void main(String[] args) {
        // With a little effort, the SJT algorithm can be modified to
        // produce permuations of all the prefixes of the main list,
        // which precludes the need this loop.
        for (int d = 9; d > 1; d--) {
            final Common.MutableInt largest = new Common.MutableInt();
            final int digits = d;
            Common.sjtPermutations(d, permutation -> {
                int number = (int)Common.digitArrayToNumber(permutation, 0,
                        digits, 1);
                boolean isPrime = Common.isPrime((long)number).getFirst();
                if (isPrime && number > largest.value) {
                    largest.value = number;
                }
            });
            if (largest.value != 0) {
                System.out.println(largest.value);
                break;
            }
        }
    }
}
