import java.math.BigInteger;

public class Problem0020 {
    public static void main(String[] args) {
        BigInteger product = BigInteger.ONE;
        for (int i = 2; i <= 100; i++) {
            product = product.multiply(BigInteger.valueOf((long)i));
        }
        int sumDigits = 0;
        BigInteger ten = BigInteger.valueOf(10L);
        while (!product.equals(BigInteger.ZERO)) {
            BigInteger[] divAndRemainder = product.divideAndRemainder(ten);
            product = divAndRemainder[0];
            sumDigits += divAndRemainder[1].intValue();
        }
        System.out.println(sumDigits);
    }
}
