import java.util.HashMap;

public class Problem0026 {
    public static void main(String[] args) {
        int longest = 0, longestD = 0;
        for (int d = 2; d < 1000; d++) {
            HashMap<Integer,Integer> remainders = new HashMap<>();
            int n = 1, i = 0;
            while (n > 0) {
                n *= 10;
                i++;
                while (n < d) {
                    n *= 10;
                    i++;
                }
                if (remainders.containsKey(n)) {
                    int period = i - remainders.get(n);
                    if (period > longest) {
                        longest = period;
                        longestD = d;
                    }
                    break;
                }
                remainders.put(n,i);
                n = n % d;
            }
        }
        System.out.println(longestD);
    }
}
