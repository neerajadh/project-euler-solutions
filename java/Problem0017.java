import java.util.Arrays;

public class Problem0017 {
    public static void main(String[] args) {
        String[] oneToNine = { "one", "two", "three", "four", "five", "six",
            "seven", "eight", "nine"
        };
        String[] tens = { "ten", "twenty", "thirty", "forty", "fifty", "sixty",
            "seventy", "eighty", "ninety" };
        String[] elevenToNineteen = { "eleven", "twelve", "thirteen",
            "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
            "nineteen" };
        String hundred = "hundred";
        String thousand = "thousand";
        String and = "and";

        int sum = 0;
        int oneToNineSum = 0;
        for (String str : oneToNine) {
            oneToNineSum += str.length();
        }
        int elevenToNineteenSum = 0;
        for (String str : elevenToNineteen) {
            elevenToNineteenSum += str.length();
        }
        sum += oneToNineSum + tens[0].length() + elevenToNineteenSum;
        for (String str : Arrays.asList(tens).subList(1,tens.length)) {
            sum += str.length() * 10 + oneToNineSum;
        }
        sum *= 10;
        sum += (hundred.length() + and.length()) * 900;
        sum -= (9 * and.length());
        sum += oneToNineSum * 100;
        sum += oneToNine[0].length() + thousand.length();
        
        System.out.println(sum);
    }
}
