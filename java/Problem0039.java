public class Problem0039 {
    public static void main(String[] args) {
        final int limit = 1000;
        // We only need an array of 500 because the sum of every
        // pythagorean triple is even
        int[] solutions = new int[limit/2];
        for (int n = 1; n < limit; n++) {
            // m i odd when n is even and vice versa, because m and n
            // cannot be both odd or both even
            for (int m = n + 1; m < limit; m += 2) {
                // when the triple is generated using euler's formula,
                // the sum is 2m(m+n)
                int s = 2 * m * (m + n);
                if (s > limit) break;
                // m and n should be coprime for the triple to be primitive
                if (Common.gcd(m,n) > 1) continue;
                solutions[(s / 2) - 1]++;
                // Increment values for the multiples to reach the
                // non-primitive triples
                for (int i = 2; i * s <= limit; i++) {
                    solutions[(i * s / 2) - 1]++;
                }
            }
        }
        int highest = 0, highestI = 0;
        for (int i = 0; i < limit / 2; i++) {
            if (solutions[i] > highest) {
                highest = solutions[i];
                highestI = i;
            }
        }
        System.out.println((highestI + 1) * 2);
    }
}
