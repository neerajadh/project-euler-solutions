import java.util.Map;
import java.util.HashSet;
import java.util.SortedMap;

public class Problem0029 {
    public static void main(String[] args) {
        final int min_a = 2, max_a = 100, min_b = 2, max_b = 100;
        HashSet<String> uniquePowers = new HashSet<>();
        for (int a = min_a; a <= max_a; a++) {
            for (int b = min_b; b <= max_b; b++) {
                StringBuilder powers = new StringBuilder();
                SortedMap<Integer,Integer> factorization = Common.factorize(a);
                for (Integer factor : factorization.keySet()) {
                    powers.append(String.format("%d^%d,",
                                factor,
                                (factorization.get(factor) * b)));
                }
                uniquePowers.add(powers.toString());
            }
        }
        System.out.println(uniquePowers.size());
    }
}
