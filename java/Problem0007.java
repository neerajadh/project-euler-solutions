import java.util.BitSet;

public class Problem0007 {
    public static void main(String[] args) {
        // The prime number theorem says that, asymptotically, there will be
        // p(N) ~ N/log(N) primes less than N. p(N) comes out to be about
        // 10260 for N = 120000. This isn't really a good solution, because
        // this value is hard-coded.
        BitSet primes = Common.primeSieve(120000);
        int count = 1;
        int prime = 2;
        while (count < 10001) {
            prime = primes.nextSetBit(prime + 1);
            count++;
        }
        System.out.println(prime);
        System.out.println(count);
    }
}
