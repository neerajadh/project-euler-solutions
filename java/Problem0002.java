public class Problem0002 {
    public static void main(String[] args) {
        // Let's do this the straightforward way
        final int limit = 4000000;
        int sum = 0;
        int a = 0, b = 1, c = 1;
        while (c < limit) {
            c = a + b;
            a = b;
            b = c;
            sum += (c % 2 == 0) ? c : 0;
        }
        System.out.println(sum);
    }
}
