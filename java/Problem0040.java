public class Problem0040 {
    public static void main(String[] args) {
        int product = 1;
        for (int i = 0; i <= 6; i++) {
            product *= getDigitAtPosition((int)Math.pow(10,i));
        }
        System.out.println(product);
    }

    private static int getDigitAtPosition(int n) {
        int digitsBefore = 0, nDigits = 1;
        while (true) {
            int digits = nDigits * 9 * (int)Math.pow(10, nDigits - 1);
            if (digitsBefore + digits > n) {
                break;
            }
            digitsBefore += digits;
            nDigits++;
        }
        //System.out.println(digitsBefore);
        //System.out.println(nDigits);
        n -= (digitsBefore + 1);
        //System.out.println(n);
        int numberWithDigit = (int)Math.pow(10,nDigits - 1) + (n / nDigits);
        int indexInNumber = n % nDigits;
        //System.out.println(numberWithDigit);
        //System.out.println(indexInNumber);
        return ithDigit(numberWithDigit,indexInNumber);
    }

    private static int ithDigit(int number, int i) {
        int digitCount = Common.digitCount(number);
        if (i < 0 || i >= digitCount) {
            throw new ArrayIndexOutOfBoundsException("i = " + i);
        }
        while (number > 0) {
            digitCount--;
            if (digitCount == i) {
                return number % 10;
            }
            number /= 10;
        }
        return -1;
    }
}
