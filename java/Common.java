import java.util.BitSet;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.function.Consumer;

public class Common {

    public static class Pair<X,Y> {
        private X first;
        private Y second;

        public Pair(X first, Y second) {
            set(first,second);
        }

        public X getFirst() {
            return first;
        }
        
        public Y getSecond() {
            return second;
        }

        public void setFirst(X first) {
            this.first = first;
        }

        public void setSecond(Y second) {
            this.second = second;
        }

        public void set(X first, Y second) {
            this.first = first;
            this.second = second;
        }
    }

    public static class MutableInt {
        public int value = 0;
    }


    public static class MutableLong {
        public long value = 0;
    }

    /**
     * Run the Sieve of Eratosthenes to find the prime numbers
     * up to size (exclusive).
     *
     * @param size  size of the bit set returned
     * @return      a bitset of size <code>size</code>,
     *              in which bits are set at prime indices
     */
    public static BitSet primeSieve(int size) {
        BitSet primes = new BitSet(size);
        primes.clear(0);
        primes.clear(1);
        primes.set(2,size);
        int i = 2;
        while (i * i  < size) {
            int m = 2 * i;
            while (m < size) { 
                primes.clear(m);
                m += i;
            }
            i++;
            while (!primes.get(i)) i++;
        }
        return primes;
    }

    /**
     * Test whether a number is prime.
     *
     * @param num   the number to test for primality
     * @return      a <code>Pair&lt;Boolean,Long&gt;<code/> where the
     *              first element indicates prime-ness and the
     *              second is a divisor of the number. If <code>num</code>
     *              is non-prime, the divisor will will be the smallest
     *              non-trivial divisor of <code>num</code>.
     */
    public static Pair<Boolean,Long> isPrime(Long num) {
        Pair<Boolean,Long> result = new Pair<>(true,num);
        if (num <= 1) {
            result.setFirst(false);
            return result;
        }
        if (num % 2 == 0) {
            if (num == 2) {
                result.setFirst(true);
            } else {
                result.set(false,2L);
            }
            return result;
        }
        long i = 3;
        while (i * i <= num) {
            if (num % i == 0) {
                result.set(false,i);
                break;
            }
            i += 2;
        }
        return result;
    }

    /**
     * Calculate the greatest common divisor of two numbers.
     */
    public static int gcd(int a, int b) {
        if (a < b) {
            int t = a;
            a = b;
            b = t;
        }
        if (b == 0) return a;
        if (a % b == 0) return b;
        else return gcd(b, a % b);
    }


    /**
     * Compute the prime factorization of the given number by trial division
     * and return it in a map.
     *
     * @param num   The number to calculate the prime factorization of
     * @return      A <code>SortedMap&lt;Integer,Integer&gt;</code> where the
     *              key is the prime factor and the value is its power. Entries
     *              are sorted by key (prime factor).
     */
    public static SortedMap<Integer,Integer> factorize(int num) {
        SortedMap<Integer,Integer> map = new TreeMap<>();
        if (num == 1) {
            return map;
        }
        int d = 2;
        while (num > 1) {
            if (d * d > num) {
                map.put(num,1);
                break;
            }
            while (num % d == 0) {
                num /= d;
                if (map.containsKey(d)) {
                    map.put(d,map.get(d) + 1);
                } else {
                    map.put(d,1);
                }
            }
            d += d == 2 ? 1 : 2;
        }
        return map;
    }

    /**
     * Return a list containing all combinations of size r of elements from
     * the given list, when the combinations are taken with replacement.
     * The size of the returned list will be C(N+r-1,r) where <code>N</code>
     * is the size of <code>elements</code>. The elements of each combination
     * will be ordered in the same order they occur in <code>elements</code>.
     *
     * @param elements  The elements from which to take combinations.
     * @param r         Size of the taken combinations
     * @return          List of all multi-combinations of size r taken from
     *                  the given list.
     */
    public static <T> List<List<T>> multiCombinations(List<T> elements,
            int r) {
        LinkedList<List<T>> combinations = new LinkedList<>();
        for (T element : elements) {
            List<T> initComb = new ArrayList<>();
            initComb.add(element);
            combinations.add(initComb);
        }
        for (int s = 2; s <= r; s++) {
            while (combinations.peek().size() == s - 1) {
                List<T> comb = combinations.pop();
                int lastPosition = elements.indexOf(comb.get(comb.size() - 1));
                for (T element : elements) {
                    if (elements.indexOf(element) >= lastPosition) {
                        ArrayList<T> combCopy = new ArrayList<>(comb);
                        combCopy.add(element);
                        combinations.add(combCopy);
                    }
                }
            }
        }
        return combinations;
    }

    /** Find out if a given number is a palindrome in a given base.
     *
     * @param n     the number to check for palindrome-ness
     * @param base  the base in which to check for palindrome-ness
     * @return      true if the number is a palindrome in the given base,
     *              false if not
     */
    public static boolean isPalindrome(int n, int base) {
        int m = 0;
        if (n % base == 0) return false;
        while (m <= n) {
            m = (m * base) + (n % base);
            if (m == n) return true;
            n /= base;
            if (m == n) return true;
        }
        return false;
    }

    public static int digitCount(long n) {
        int d = 0;
        while (n > 0L) {
            d++;
            n /= 10L;
        }
        return d;
    }

    public static void swapIntArrayElements(int[] arr, int i, int j) {
        int t = arr[i];
        arr[i] = arr[j];
        arr[j] = t;
    }

    /**
     * Generate all permuations of the list (0, 1, 2, 3 , , , n - 1) using the 
     * Steinhaus-Johnson-Trotter algorithm. The generated permutations are
     * supplied one by one to the provided consumer.
     */
    public static void sjtPermutations(int n, Consumer<int[]> consumer) {
        int[] directions = new int[n];
        int[] arr = new int[n];
        int[] indexOf = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = i;
            indexOf[i] = i;
            directions[i] = -1;
        }
        while (true) {
            consumer.accept(arr);
            int toMove = -1;
            for (int i = n - 1; i > 0; i--) {
                int nextIndex = indexOf[i] + directions[i];
                if (nextIndex >= 0 && nextIndex < n && arr[nextIndex] < i) {
                    toMove = i;
                    break;
                }
            }
            if (toMove == -1) {
                break;
            } else {
                Common.swapIntArrayElements(arr, indexOf[toMove],
                        indexOf[toMove] + directions[toMove]);
                Common.swapIntArrayElements(indexOf, toMove,
                        arr[indexOf[toMove]]);
                for (int i = toMove + 1; i < n; i++) {
                    directions[i] = -1 * directions[i];
                }
            }
        }
    }

    public static long digitArrayToNumber(int[] digitArray, int start, int end,
            int shift) {
        long value = 0;
        for (int i = start; i < end; i++) {
            value = value * 10L + (long) (digitArray[i] + shift);
        }
        return value;
    }
    
    public static void main(String[] args) {
        BitSet primes = primeSieve(100);
        for (int i = 0; i < 100; i++) {
            String output = i + ": " + (primes.get(i) ? "prime" : "non-prime");
            Pair<Boolean,Long> primeTest = isPrime((long)i);
            if (!primeTest.getFirst()) {
                output += " , divisor = " + primeTest.getSecond();
            }
            System.out.println(output);
            assert primes.get(i) == primeTest.getFirst();
        }
        assert gcd(5,5) == 5;
        assert gcd(5,0) == 5;
        assert gcd(4,4) == 4;
        assert gcd(17,19) == 1;
        assert gcd(14,21) == 7;
        assert gcd(30,45) == 15;

        Map<Integer,Integer> factors = factorize(210);
        System.out.println(factors);
        assert factors.size() == 4;
        factors = factorize(2340);
        System.out.println(factors);
        assert factors.size() == 4;
        assert factors.containsKey(2) && factors.containsKey(3)
            && factors.containsKey(5) && factors.containsKey(13);
        assert factors.get(2) == 2 && factors.get(3) == 2
            && factors.get(5) == 1 && factors.get(13) == 1;
        factors = factorize(1);
        System.out.println(factors);
        assert factors.size() == 0;
        factors = factorize(53);
        System.out.println(factors);
        assert factors.size() == 1 && factors.containsKey(53)
            && factors.get(53) == 1;
    }
}
