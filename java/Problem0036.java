import java.util.Arrays;

public class Problem0036 {
    public static void main(String[] args) {
        long sum = 0;
        for (int d = 1; d <= 10; d++) {
            for (int b = 1; b < (1 << d); b += 2) {
                int rev = binaryReverse(b);
                int shift = d + (d - binaryDigitCount(b));
                int palindromeE = (rev << shift) + b;
                int palindromeO0 = (rev << (shift + 1)) + b;
                int palindromeO1 = palindromeO0 + (1 << d);
                for (Integer i : Arrays.asList(palindromeE,palindromeO0,
                            palindromeO1)) {
                    if (Common.isPalindrome(i,10) && i < 1000000) {
                        sum += (long) i;
                    }
                }
            }
        }
        System.out.println(sum);
    }

    private static int binaryReverse(int n) {
        int r = 0;
        while (n > 0) {
            r = (r << 1) + (n % 2);
            n /= 2;
        }
        return r;
    }

    private static int binaryDigitCount(int n) {
        int d = 0;
        while (n > 0) {
            d++;
            n /= 2;
        }
        return d;
    }
}
